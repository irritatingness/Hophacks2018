# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180218035958) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.bigint "charity_api_id"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "charity_organizations", force: :cascade do |t|
    t.string "name"
    t.bigint "cause_id"
    t.text "mission"
    t.string "website"
    t.bigint "charity_api_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "trustworthiness"
    t.bigint "preference_id"
  end

  create_table "preferences", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "charity_api_id"
    t.bigint "category_id"
  end

  create_table "preferences_users", id: false, force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "preference_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email"
    t.string "password_hash"
    t.string "password_salt"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "temp_token"
    t.datetime "temp_created"
  end

end
