class AddCharityApiIdToPreferences < ActiveRecord::Migration[5.1]
  def change
  	add_column :preferences, :charity_api_id, :bigint
  end
end
