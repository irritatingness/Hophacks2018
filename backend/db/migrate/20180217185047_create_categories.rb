class CreateCategories < ActiveRecord::Migration[5.1]
  def change
    create_table :categories do |t|
      t.string :name
      t.bigint :charity_api_id
      t.text :description

      t.timestamps
    end
  end
end
