class AddTempTokenToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :temp_token, :string
    add_column :users, :temp_created, :datetime
  end
end
