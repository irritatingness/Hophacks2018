class AddTrustWorthinessToCharityOrganizations < ActiveRecord::Migration[5.1]
  def change
    add_column :charity_organizations, :trustworthiness, :integer
  end
end
