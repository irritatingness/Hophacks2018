class AddCategoryIdToPreferences < ActiveRecord::Migration[5.1]
  def change
    add_column :preferences, :category_id, :bigint
  end
end
