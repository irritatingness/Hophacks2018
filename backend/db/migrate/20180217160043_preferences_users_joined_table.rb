class PreferencesUsersJoinedTable < ActiveRecord::Migration[5.1]
  def change
  	create_table :preferences_users, id: false do |t|
  		t.bigint :user_id
  		t.bigint :preference_id
  	end
  end
end
