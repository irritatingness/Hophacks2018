class CreateCharityOrganizations < ActiveRecord::Migration[5.1]
  def change
    create_table :charity_organizations do |t|
      t.string :name
      t.bigint :cause_id
      t.text :mission
      t.string :website
      t.bigint :charity_api_id

      t.timestamps
    end
  end
end
