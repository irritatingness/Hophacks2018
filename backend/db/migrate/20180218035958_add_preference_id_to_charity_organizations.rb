class AddPreferenceIdToCharityOrganizations < ActiveRecord::Migration[5.1]
  def change
    add_column :charity_organizations, :preference_id, :bigint
  end
end
