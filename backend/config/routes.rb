Rails.application.routes.draw do
  
  resources :charity_organizations
  resources :categories
  resources :preferences, only: [:index]
  resources :sessions, only: [:create, :destroy]
  get "discover" => "charity_organizations#discover", as: "discover"

  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
