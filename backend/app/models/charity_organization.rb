class CharityOrganization < ApplicationRecord
	belongs_to :preference
	def self.get_from_cache_or_fetch(user)
		# organizations we don't have cached
		fetch_ids = []
		# populate a list of preferences to fetch
		user.preferences.each do |pref|
			if pref.charity_organizations.blank?
				fetch_ids << [pref.charity_api_id, pref.id]
			end
		end
		# if any, fetch from api
		if fetch_ids.any?
			self.fetch_by_cause(fetch_ids)
		end
		# compile a list of organizations that fits preferences
		@charities = CharityOrganization.where(preference_id: user.preferences.pluck(:id))
	end

	def self.fetch_by_cause(ids)
		ids.each do |id|
			uri = 'https://api.data.charitynavigator.org/v2/Organizations?app_id=f942e24b&app_key=e1fdeddeb32086f78fe5e34495bdee79&causeID=' + id[0].to_s
			response = RestClient.get uri
			JSON.parse(response.body).each do |org|
				CharityOrganization.create(name: org["charityName"], 
										   preference_id: id[1], 
										   mission: org["mission"],
										   website: org["websiteURL"],
										   charity_api_id: org["orgID"],
										   trustworthiness: org["currentRating"]["score"])
			end
		end
	end
end

