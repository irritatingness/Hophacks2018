class Preference < ApplicationRecord
	has_and_belongs_to_many :users
	belongs_to :category
	has_many :charity_organizations
end
