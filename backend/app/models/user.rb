class User < ApplicationRecord
	attr_accessor :password
	validates_presence_of :email, on: :create
	validates_presence_of :password, on: :create
	validates_confirmation_of :password
	validates_uniqueness_of :email
	before_save :downcase_email
	before_create :hash_password
	before_create :assign_causes
	validate :password_format, on: :create
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
    validates :email, length: { maximum: 255 }, uniqueness: { case_sensitive: false }, 
                                        format: { with: VALID_EMAIL_REGEX }
    has_and_belongs_to_many :preferences


	

	def generate_token
		self.temp_token = BCrypt::Engine.generate_salt
		self.temp_created = Date.today
		self.save(validate:false)
	end

	def assign_causes
		ids = []
		prefs = Preference.pluck(:id)
		ids[0] = prefs.sample
		ids[1] = prefs.sample
		self.preference_ids = ids
	end

	def expired_token?
		self.temp_created + 1.days < Date.today
	end

	def token_mismatch?(token)
		self.temp_token != token
	end

	private

	

	def downcase_email
		self.email = self.email.downcase 
	end

	def password_format
		if password.blank?
			errors.add :base, "Password field may not be empty"
		end
	end

	def hash_password
        if password.present?
            self.password_salt = BCrypt::Engine.generate_salt
            self.password_hash = User.digest(password,password_salt)
        end
    end

    def User.digest(string, salt=nil)
        if !salt
            salt = BCrypt::Engine.generate_salt
        end
            hashed_string = BCrypt::Engine.hash_secret(string, salt)
    end


    def self.authenticate(email,password)
        user = find_by_email(email.downcase)
        if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
            user
        else
            nil
        end
    end


end
