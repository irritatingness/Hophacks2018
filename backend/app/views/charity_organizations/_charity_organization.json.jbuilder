json.extract! charity_organization, :id, :name, :cause_id, :mission, :website, :charity_api_id, :created_at, :updated_at
json.url charity_organization_url(charity_organization, format: :json)
