json.extract! category, :id, :name, :charity_api_id, :description, :created_at, :updated_at
json.url category_url(category, format: :json)
