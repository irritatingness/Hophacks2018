class SessionsController < ApplicationController
  skip_before_action :verify_authenticity_token
  
  def new
  end
  
  def create
    user = User.authenticate(params[:email], params[:password])
    if user
      user.generate_token
      respond_to do |format|
        format.json { render json: user.as_json(only: [:temp_token, :id])}
      end
    else
      respond_to do |format|
        format.json { render json: "rip" }
      end
    end
  end
  
  def destroy 
    user = User.find_by(token: params[:token])
    if user
      user.temp_token = nil
      user.temp_created = nil
      user.save(validate: false)
    end
  end
end
