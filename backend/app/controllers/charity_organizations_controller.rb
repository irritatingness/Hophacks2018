class CharityOrganizationsController < ApplicationController
  before_action :authorize

  def index
  		user = User.find_by(temp_token: params[:token])
		if user.blank?
  			respond_to do |format|
  				format.json { render json: :forbidden }
  			end
		else
    			@charities = CharityOrganization.get_from_cache_or_fetch(user).limit(10)
    			respond_to do |format|
      				format.json { render json: @charities.as_json(only: [:name, :website, :mission, :trustworthiness])}
    			end
		end
	end

  def discover
    ids = CharityOrganization.pluck(:id)
    max = ids.max
    min = ids.min
    @charities = []
    5.times do
      @charities << CharityOrganization.where(id: rand(min..max)).first
    end
    respond_to do |format|
      format.json { render json: @charities }
    end
  end
end
