class PreferencesController < ApplicationController
  before_action :authorize
  before_action :set_preference, only: [:show, :edit, :update, :destroy]

  # GET /preferences
  # GET /preferences.json
  def index
    @preferences = Preference.all
    respond_to do |format|
      format.json {render json: @preferences.map { |pref| [pref.name, pref.id, pref.user_ids] } }
    end
  end



  private
    # Use callbacks to share common setup or constraints between actions.
    def set_preference
      @preference = Preference.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def preference_params
      params.require(:preference).permit(:name)
    end
end
