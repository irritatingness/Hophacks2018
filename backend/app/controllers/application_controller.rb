class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception


  def authorize
  	user = User.find_by(temp_token: params[:token])
  	if !user || user.expired_token?
  		respond_to do |format|
  			format.json { render json: "rip\n" }
  		end
  	end
  end
end
