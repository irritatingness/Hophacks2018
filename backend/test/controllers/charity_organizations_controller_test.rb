require 'test_helper'

class CharityOrganizationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @charity_organization = charity_organizations(:one)
  end

  test "should get index" do
    get charity_organizations_url
    assert_response :success
  end

  test "should get new" do
    get new_charity_organization_url
    assert_response :success
  end

  test "should create charity_organization" do
    assert_difference('CharityOrganization.count') do
      post charity_organizations_url, params: { charity_organization: { cause_id: @charity_organization.cause_id, charity_api_id: @charity_organization.charity_api_id, mission: @charity_organization.mission, name: @charity_organization.name, website: @charity_organization.website } }
    end

    assert_redirected_to charity_organization_url(CharityOrganization.last)
  end

  test "should show charity_organization" do
    get charity_organization_url(@charity_organization)
    assert_response :success
  end

  test "should get edit" do
    get edit_charity_organization_url(@charity_organization)
    assert_response :success
  end

  test "should update charity_organization" do
    patch charity_organization_url(@charity_organization), params: { charity_organization: { cause_id: @charity_organization.cause_id, charity_api_id: @charity_organization.charity_api_id, mission: @charity_organization.mission, name: @charity_organization.name, website: @charity_organization.website } }
    assert_redirected_to charity_organization_url(@charity_organization)
  end

  test "should destroy charity_organization" do
    assert_difference('CharityOrganization.count', -1) do
      delete charity_organization_url(@charity_organization)
    end

    assert_redirected_to charity_organizations_url
  end
end
