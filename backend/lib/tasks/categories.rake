require 'rest-client'

namespace :categories do
     desc 'Populate database with categories'
     task :fetch_categories => :environment do
     	response = RestClient.get 'https://api.data.charitynavigator.org/v2/categories?app_id=f942e24b&app_key=e1fdeddeb32086f78fe5e34495bdee79&pageNum=10'
     	JSON.parse(response.body).each do |category|
     		cat = Category.find_or_create_by(name: category["categoryName"], charity_api_id: category["categoryID"], description: category["image"])
     		category["causes"].each do |cause|
     			cat.preferences.create(name: cause["causeName"], charity_api_id: cause["causeID"])
     		end
     	end
     end
 end
