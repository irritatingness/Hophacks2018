import React from 'react';
import { View, Text, StyleSheet, LayoutAnimation } from 'react-native';
import { NavigationActions } from 'react-navigation';
import Touchable from 'react-native-platform-touchable';
import { Ionicons } from '@expo/vector-icons';

export default class TouchablePreference extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      bool: this.props.data[2]
    }
    this.handlePress = this.handlePress.bind(this);
  }

  handlePress() {
    LayoutAnimation.spring();
    this.setState({
      bool: !this.state.bool
    });
  }

  render() {
    //console.log(this.props);
    return (
      <View>
        <Touchable
          style={styles.option}
          background={Touchable.Ripple('#ccc', false)}
          onPress={this.handlePress}
        >
          <View style={{ flexDirection: 'row' }}>
            <View style={{ width: '80%' }}>
              <Text style={styles.optionsTitleText}>
                {this.props.data[0]}
              </Text>
            </View>
            <View style={{ display: (this.state.bool ? 'flex' : 'none'), alignSelf: 'flex-end', width: '10%' }}>
              <Ionicons name="md-checkmark" color="green" size={32} />
            </View>
          </View>
        </Touchable>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
  },
  optionsTitleText: {
    fontSize: 16,
    marginLeft: 10,
    marginTop: 9,
    marginBottom: 12
  },
  optionIconContainer: {
    marginRight: 9,
  },
  option: {
    backgroundColor: '#fdfdfd',
    paddingHorizontal: 15,
    paddingVertical: 15,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#EDEDED',
  },
  optionText: {
    fontSize: 15,
    marginTop: 1,
  },
});
