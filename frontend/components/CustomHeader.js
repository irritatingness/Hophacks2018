import React from 'react';
import { View, Text } from 'react-native';
import { Header } from 'react-native-elements';
import { Ionicons } from '@expo/vector-icons';
import { NavigationActions } from 'react-navigation';

export default class CustomHeader extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      //userId: this.props.navigation.state.params.userId,
      //token: this.props.navigation.state.params.token
    }
    this.menuItemPressed = this.menuItemPressed.bind(this);
  }

  menuItemPressed() {
    //console.log("Drawer should be toggling!");
    //console.log(this.props);
    this.props.navigation.navigate("DrawerOpen");
  }

  render() {
    //console.log(this.props);
    return (
      <View>
        <Header
          backgroundColor='#07BD7B'
          leftComponent={<Ionicons name="md-menu" size={32} color="white" onPress={this.menuItemPressed}/>}
          centerComponent={<Text style={{ fontSize: 20, marginBottom: '5%', fontWeight: 'bold', color: 'white' }}>Cause Connection</Text>}
        />
      </View>
    );
  }
}
