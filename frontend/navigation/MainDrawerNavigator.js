import React from 'react';
import HomeScreen from '../screens/HomeScreen';
import PreferencesScreen from '../screens/PreferencesScreen';
import CharitiesScreen from '../screens/CharitiesScreen';

import { DrawerNavigator } from 'react-navigation';

const RootDrawerNavigator = DrawerNavigator(
  {
    Home: {
      //screen: HomeScreen
      screen: (props) => <HomeScreen {...props} />
    },
    Preferences: {
      screen: (props) => <PreferencesScreen {...props} />
    },
    Charities: {
      screen: (props) => <CharitiesScreen {...props} />
    },
  },
  {
    drawerWidth: 300
  },
  {
    headerMode: 'screen',
    navigationOptions: {
      header: null
    }
  }
);

export default class MainDrawerNavigator extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      token: this.props.navigation.state.params.token,
      userId: this.props.navigation.state.params.userId
    }
    //console.log("State in Drawer Navigator");
  }

  render() {
    console.log(this.props);
    return (
      <RootDrawerNavigator screenProps={{ token: this.state.token, userId: this.state.userId }} />
    );
  }
}
