import React from 'react';
import { View, Text, ScrollView } from 'react-native';
import CustomHeader from '../components/CustomHeader';
import TouchablePreference from '../components/TouchablePreference';


export default class CharitiesScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      token: this.props.screenProps.token,
      userId: this.props.screenProps.userId,
      charities: [],
    }

  }

  componentWillMount() {
    fetch('http://34.239.217.239/charity_organizations?token=' + this.state.token, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    })
    .then((response) => response.json())
    .then((responseJson) => {
      console.log(responseJson);
      var myArray = [];
      for (org of responseJson) {
        myArray.push([org.name, org.website, org.trustworthiness]);
      }
      this.setState({ charities: myArray});
      console.log(this.state.charities);
    })
    .catch((error) => {
      console.log(error);
    });
  }

  makeCharities() {
    var myArray = [];
    for (var i = 0; i < this.state.charities.length; i++) {
      myArray.push(<Text style= {{ fontSize: 16 }} key={i}>{ this.state.charities[i][0]}</Text>);
    }
    return myArray;
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <CustomHeader navigation={this.props.navigation} />
        <ScrollView>
          {this.makeCharities()}
        </ScrollView>
      </View>
    );
  }
}
