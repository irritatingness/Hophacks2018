import React from 'react';
import { View, Text, ScrollView } from 'react-native';
import CustomHeader from '../components/CustomHeader';
import TouchablePreference from '../components/TouchablePreference';


export default class PreferencesScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      token: this.props.screenProps.token,
      userId: this.props.screenProps.userId,
      preferences: [],
    }

  }

  componentWillMount() {
    fetch('http://34.239.217.239/preferences?token=' + this.state.token, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    })
    .then((response) => response.json())
    .then((responseJson) => {
      var myArray = [];
      for (pref of responseJson) {
        myArray.push([pref[0], pref[1], pref[2].includes(this.state.userId)]);
      }
      this.setState({ preferences: myArray});
      console.log(this.state.preferences);
    })
    .catch((error) => {
      console.log(error);
    });
  }

  makePreferences() {
    var myArray = [];
    for (var i = 0; i < this.state.preferences.length; i++) {
      myArray.push(<TouchablePreference data={this.state.preferences[i]} key={i} />);
    }
    return myArray;
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <CustomHeader navigation={this.props.navigation} />
        <ScrollView>
          {this.makePreferences()}
        </ScrollView>
      </View>
    );
  }
}
