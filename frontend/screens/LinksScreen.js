import React from 'react';
import { View, Text, ScrollView } from 'react-native';
import CustomHeader from '../components/CustomHeader';

export default class LinksScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      token: this.props.screenProps.token,
      userId: this.props.screenProps.userId
    }
    fetch('http://34.239.217.239/charity_organizations?token=' + this.state.token + '&user_id=' + this.state.userId, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    })
    .then((response) => response.json())
    .then((responseJson) => {
      console.log(responseJson);
    })
    .catch((error) => {
      console.log(error);
    });

    /*
    fetch('http://34.239.217.239/preferences?token=' + this.state.token, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    })
    .then((response) => response.json())
    .then((responseJson) => {
      console.log(responseJson);
    })
    .catch((error) => {
      console.log(error);
    });
    */
  }



  render() {
    return (
      <View style={{ flex: 1 }}>
        <CustomHeader navigation={this.props.navigation} />
        <ScrollView>

        </ScrollView>
      </View>
    );
  }
}
