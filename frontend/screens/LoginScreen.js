import React from 'react';

import {
  Image,
  Text,
  View,
  StatusBar,
} from 'react-native';

import { NavigationActions } from 'react-navigation';

import { FormLabel, FormInput, Button } from 'react-native-elements';

import { LinearGradient } from 'expo';

export default class LoginScreen extends React.Component {

  static navigationOptions = {
    header: null,
  };

  constructor() {
    super();
    this.loginButtonPressed = this.loginButtonPressed.bind(this);
    this.signupButtonPressed = this.signupButtonPressed.bind(this);
  }

  state = {
    email: "",
    password: "",
    token: ""
  }

  render() {
    return(
      <View style={{flex:1}}>
      <StatusBar hidden={true}/>
        <LinearGradient colors={['#00aee8', '#0ecc0e']} style={{ flex: 1 }}>
          <View style={{ flex:1, justifyContent: 'center' }}>
            <Text size={32} style={{ fontSize: 32, textAlign: 'center', marginBottom: '25%', fontWeight: 'bold', color: 'white' }}>Cause{'\n'}Connection</Text>
            <FormInput inputStyle={{ color: 'black' }} placeholder="Email" onChangeText={(text) => this.setState({email: text})}></FormInput>
            <FormInput inputStyle={{ color: 'black', marginTop: '10%' }} placeholder="Password" secureTextEntry={true} onChangeText={(text) => this.setState({password: text})}></FormInput>
            <View
              style={{ justifyContent: 'center', flexDirection: 'row', marginTop: '10%' }}
            >
              <Button
                title='Login'
                backgroundColor='#06C496'
                onPress={this.loginButtonPressed}
              />
              <Button
                title='Register'
                backgroundColor='#06C496'
                onPress={this.signupButtonPressed}
              />
            </View>
          </View>
        </LinearGradient>
      </View>
    );
  }

  loginButtonPressed() {
    //console.log("Button was pressed!");

    fetch('http://34.239.217.239/sessions', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        "email" : this.state.email,
        "password": this.state.password
      })
    })
    .then((response) => response.json())
    .then((responseJson) => {
      //console.log(responseJson);
      this.setState({ token: responseJson.temp_token, userId: responseJson.id});
      //console.log("Token: " + this.state.token);
      const navigateAction = NavigationActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'Home', params: { token: this.state.token, userId: this.state.userId } })],
      });
      this.props.navigation.dispatch(navigateAction);
    })
    .catch((error) => {
      console.log(error);
    });

    /*
    return fetch('https://facebook.github.io/react-native/movies.json')
    .then((response) => response.json())
    .then((responseJson) => {
      return responseJson.movies;
    })
    .catch((error) => {
      console.error(error);
    });
    */
  }

  /*
  updateButtonPressed() {
    console.log("Button was pressed!");
    //alert("Hi!");

    fetch('http://34.239.217.239/users/4', {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        "token": "$2a$10$gOcyfDKcvJvU7lOLEZkpp.",
        user: {
          "preference_ids": [1,2]
        }
      })
    })
    .then((response) => response.json())
    .then((responseJson) => {
      console.log(responseJson);
      this.setState({ token: responseJson.temp_token});
      console.log(this.state.token);
      const navigateAction = NavigationActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'Home', params: { token: this.state.token, userId: this.state.email } })],
      });
      this.props.navigation.dispatch(navigateAction);
    })
    .catch((error) => {
      console.log(error);
    });

    /*
    return fetch('https://facebook.github.io/react-native/movies.json')
    .then((response) => response.json())
    .then((responseJson) => {
      return responseJson.movies;
    })
    .catch((error) => {
      console.error(error);
    });

  }
  */

  signupButtonPressed() {
    //console.log("Button was pressed!");

    fetch('http://34.239.217.239/users', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        user: {
          "email" : this.state.email,
          "password": this.state.password,
          "password_confirmation": this.state.password
        }
      })
    })
    .then((response) => response.json())
    .then((responseJson) => {
      //console.log(responseJson);
      this.setState({ token: responseJson.temp_token});
      console.log("token: " + this.state.token);
      const navigateAction = NavigationActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'Home', params: { token: this.state.token, userId: this.state.email } })],
      });
      this.props.navigation.dispatch(navigateAction);
    })
    .catch((error) => {
      console.log(error);
    });

    /*
    return fetch('https://facebook.github.io/react-native/movies.json')
    .then((response) => response.json())
    .then((responseJson) => {
      return responseJson.movies;
    })
    .catch((error) => {
      console.error(error);
    });
    */
  }

}
