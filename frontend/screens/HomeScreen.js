import React from 'react';
import { ScrollView, View, Text } from 'react-native';
import { Button } from 'react-native-elements';
import CustomHeader from '../components/CustomHeader';
import TouchableDiscover from '../components/TouchableDiscover';

export default class HomeScreen extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      token: this.props.screenProps.token,
      userId: this.props.screenProps.userId,
      discoverObjects: [],
    }
    this.createDiscoverObjects = this.createDiscoverObjects.bind(this);
  }

  componentWillMount() {
    fetch('http://34.239.217.239/discover?token=' + this.state.token, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    })
    .then((response) => response.json())
    .then((responseJson) => {
      var myArray = [];
      for (value of responseJson) {
        myArray.push(value);
      }
      this.setState({
        discoverObjects: myArray
      });
      //console.log(responseJson);
    })
    .catch((error) => {
      console.log(error);
    });
  }

  createDiscoverObjects() {
    var myArray = [];
    for (var i = 0; i < this.state.discoverObjects.length; i++) {
      myArray.push(<TouchableDiscover data={this.state.discoverObjects[i]} key={i}/>);
      //console.log(this.state.discoverObjects[i]);
    }
    return(myArray);
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <CustomHeader navigation={this.props.navigation} />
        <ScrollView>
          {this.createDiscoverObjects()}
        </ScrollView>
      </View>
    );
  }
}
